var open=document.querySelector('.open');
var close=document.querySelector('.close');
var container=document.querySelector('.container');
// có thể thay thế bằng toggle
open.addEventListener('click', () => container.classList.add('show-nav'));
close.addEventListener('click', () => container.classList.remove('show-nav'));