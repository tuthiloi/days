# GEJO - Front Site



## Motivation 
Have the opportunity to participate in the company's project to improve knowledge, Learn a lot of experience and teamwork skills
## Code Style
- ` Indentation`
- `Classname : BEM`
## Framework used
-  `ReactJS`
## Features
- `Create account `
- `Help customers book events in advance and hire staff to do them`

## Installation 
- `Create React App`
  
  ```
  npx create-react-app my-app
  cd my-app
  npm start
  ```
## Contribute
- `Build the interface design for the page portal`
- `Design the database with team`
## Authors
- `Hoang Ha Tri` 
- `Tu Thi Loi`
## License
This project is open-source and licensed under the [MIT license](http://opensource.org/licenses/MIT)