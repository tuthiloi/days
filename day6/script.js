/* const columns = document.querySelectorAll('.column')
window.addEventListener('scroll', () => {
    const triggerBottom = window.innerHeight / 5 * 4

    columns.forEach(column => {
        const boxTop = column.getBoundingClientRect().top

        if (boxTop < triggerBottom) {
            column.classList.add('show')
        } else {
            column.classList.remove('show')
        }
    })
}) */
const boxes = document.querySelectorAll('.column')

window.addEventListener('scroll', checkBoxes)

checkBoxes()

function checkBoxes() {
    const triggerBottom = window.innerHeight / 5 * 4

    boxes.forEach(box => {
        const boxTop = box.getBoundingClientRect().top

        if(boxTop < triggerBottom) {
            box.classList.add('show')
        } else {
            box.classList.remove('show')
        }
    })
}